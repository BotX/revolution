{{Staat
|Staatsname - Amtssprache= Arkadien bzw. Arkadisches Kaiserreich
|Staatsname - Transkription=
|Staatsname - Deutsch=
|Flagge= ARK-Flagge.png
|Wappen=?-Wappen.png
|Karte=
|Amtssprache= Arkadisch
|Hauptstadt= Arkadia
|Staatsform= Konstitutionelle Monarchie
|Staatsoberhaupt= Kaiser Jubileus
|Regierungschef= Archon Valerian
|Regierungspartei= Arkadomonarchisten
|Gr�ndung= 853. n Chr
|Aufl�sung=
|Nationalfeiertag= 8. Juli (Ausrufung des Kaiserreiches)
|Nationalhymne=
|Fl�che=
|Einwohnerzahl= 262.453.000<br> (+154.123.000 Sklaven)
|Einwohnerdichte=
|Volksbezeichnung= Arkadier
|Adjektiv= Arkadisch
|Religion= Arkadische Kirche
|W�hrung= 1 Solidus (Pl. Solidi), bestehend 100 Aurei (Sg. Aureus)
|BIP=
|BIP/Ew=
|Unternehmen=
|Spieler= DM2610 [VK]
|Flaggenvorlage=ARK
|Polit. K�rzel= ARK
}}

Arkadia ist eine konstitutionelle Monarchie die sich �ber einen gro�en Teil des Nordosten Afrikas erstreckt. Sie ist eins der bev�lkerungsreichsten L�nder und eines der letzten Sklavenstaaten auf der Welt. Die Gesellschaft ist tief zwischen Arkadiern und Sklaven gespalten. Au�enpolitisch verfolgte das Land eine isolierende Politik und hielt nur sp�rlichen Kontakt zu manchen Staaten. Der Senat des Landes arbeitet eng mit dem Kaiser zusammen.

=Geographie=

=Geschichte=
==Arabische Invasion==
642 n.Chr war die r�mische Provinz �gypten unter dem Ansturm der islamischen Araber zusammengebrochen, womit eine 700 Jahre lange Periode der kulturellen und wirtschaftlichen Bl�te zu Ende ging. In den folgenden 200 Jahren litt das ganze Land unter den Islamiesierungsbem�hungen der Invasoren sowie der wirtschaftlichen Ausbeutung, mit der die Araber ihre Eroberungsfeldz�ge finanzierten. 

==Aufstand Herakleios==
Der Unmut in der Bev�lkerung wuchs immer weiter an,bis im Jahre 850 n.Chr ein junger adliger Namens Herakleios der Bev�lkerung mit seinen inspirierenden Reden den Mut gab einen Aufstand zu wagen. Die Erhebung begann in der ehemaligen Provinz Arcadia, die �rtlichen arabischen Garnisonen hatte einem solch gewaltigen Volksaufstand nichts entgegen zu setzen. Halb �gypten war bereits in der Hand der aufst�ndischen, bis der arabische Kalif h�chstpers�nlich mit �ber 50.000 Mann anmarschierte, um den Verlust seiner reichsten Provinz zu verhindern.
 
===Schlacht von Alexandria===
Vor Alexandria kam es zur Entscheidungsschlacht,die von den Aufst�ndischen unter hohen Verlusten gewonnen wurde, Herakleios pers�nlich t�tete den Kalifen dabei im Zweikampf. Die Araber st�rzten dadurch ins Chaos und waren nicht mehr in der Lage organisierten Widerstand zu leisten, so dass 853 n.Chr schlie�lich ganz �gypten befreit war und die Araber Frieden anboten.

==Gr�ndung Arkadias==
In Anlehnung an den Ursprung des Aufstands wurde das neue Reich zum arkadischen Kaiserreich getauft,mit Kaiser Herakleios an der Spitze. Die folgenden Jahrzehnte waren gepr�gt vom wirtschaftlichen Wiederaufschwung und der Ausmerzung des Islams innerhalb der Reichsgrenzen. Mit dem byzantischen Reich wurden enge Kontakte gekn�pft und bald verstand man sich als Bruderreiche. Zusammen eroberte man die Levante wieder zur�ck und dr�ngte den Islam immer weiter zur�ck. 

==S�dexpansion==
Arkadien gab sich jedoch mit dem Sinai zufrieden und konzentrierte sich auf Afrika,wo es ab 1200 n.Chr mit der Unterwerfung und Assimilierung der nubischen V�lker begann, ab 1400 folgten ihnen die �thiopischen. 
1500 n.Chr erreichte Arkadien seinen heutigen Gebietsstand,sah aber von weiteren Eroberungen ab da die Grenzen schon zu weit ausgedehnt waren und man f�rchtete das Heer w�rde zu lange brauchen um das Reich einmal zu durchqueren. Man konzentrierte sich folglich auf seine inneren Angelegenheiten und wurde zum kulturellen Mittelpunkt des gesamten �stlichen Mittelmeeraumes. 

==Goldenes Zeitalter==
1500-1700: Goldene Zeit des Arkadischen Reiches. Die Hohe Lebenserwartung und Bedarf nach Sklaven f�hrt dazu das die Arkadische Bev�lkerung schrumpft w�hrend die Anzahl der Sklaven steigt.

==Die dunkle Era==
1700-1760: Die Vergleichsweise niedrige Bev�lkerungsanzahl der Arkadier gegen�ber den Nubiern und �thiopiern sorgt f�r Spannungen sodass Arkadia als "Kranker Mann am Nil" bezeichnet wurde. Hier k�nnte man einen oder mehrere Verteidigungskriege gegen einen der S�dlichen Nachbarn einbauen der zbs nur mit Byzantischer und/oder Karthagischer Hilfe abgewehrt werden konnte. 

1760-1790: Nowgorodische H�ndler gr�nden an der Ostk�ste Handelsposten und kleinere St�dte und betreiben Handel.
1790-1800: Nowgorod versucht den S�dosten Arkadias zu kolonisieren, dazu stachelt es die Sklavenbev�lkerung abseits der K�ste auf, was zu Spannungen und inneren Unruhen f�hrt. Den Moment der Schw�che nutzt es um an der K�ste Nowgorodische Kolonialst�dte zu etablieren. 
1801-1813: Im Nowgorodisch-Arkadischen Krieg kann in einer Entscheidenden Schlacht, durch einen �berl�ufer auf Nowgorodischer Seite der die Schlachtpl�ne der Nowgorodischen Seite verkaufte, ein Sieg errungen werden welcher zum Frieden von Mnogadischu, einer der Nowgorodischen Kolonialst�dte, f�hrt. Im Friedensvertrag wird festgelegt das Nowgorod weitere Kolonialbestrebungen auf Arkadischen Boden zu unterlassen hat und Arkadia die Grenzen garantiert, wof�r es im Gegenzug den zivilen, sowie begrenzten milit�rischen, Zugang zu verschiedenen H�fen im Osten und Norden Arkadias erh�lt. Nowgorodische H�ndler in Arkadia genie�en fortan den Schutz durch den Kaiser in den ehemaligen Nowgorodischen Kolonialst�dten. Die Beziehungen zwischen Nowgorod und Arkadia bleiben angespannt und in St�dten wie [Russische Pseudonamen einsetzen] blieben gro�e Russische Viertel zur�ck.

==Sklavenaufstand und B�rgerkrieg==
ca 1840-1890: Die von Nowgorod aus angestachelten Unruhen m�nden 1837 in einem Aufstand der Sklaven und verlangen nach Unabh�ngigkeit in den von ihren bewohnten Gebieten. Aufgrund der Gr��e, dem Terrain und nicht vorhandenden Industrialisierung des Landes dauert der B�rgerkrieg lange und f�hrt dazu das Arkadia den Startschuss zur Industrialisierung verpasst. Um einen Aufstand gegen den Kaiser selber zu unterbinden muss er 1839 ein Parlament einf�hren, welches von verschiedenen Kr�ften gef�llt wird. Der B�rgerkrieg verl�uft daraufhin aufgrund der Instabilit�t des Parlamentes schlecht und die Sklaven k�nnen im S�den und Osten gewinne verzeichnen. Durch die Unterst�tzung aus Neu-Byzanz und Karthago ist ein weiterkommen in die Arkadische Kernlande aber nicht m�glich. 
1848 k�nnen Sklaventruppen die ehemaligen Nowgorodischen Handelsst�dte einnehmen. In der Stadt kommt es daraufhin zu Pogromen gegen die Nowgorodischen H�ndler und die Arkadische Bev�lkerung. Die Sklaven verbieten darauf hin Nowgorod den Zugang zu den H�fen, worauf hin dieses den Kaiser in Arkadia anf�ngt milit�risch wie finanziell zu unterst�tzen. 
1853 gewinnen, nach einem erfolgreichen und Aggressiven Wahlkampf, die Arkadisten, welche starken Zuspruch durch das Milit�r bekommen, die Wahl und formen mit den Kaisertreuen Monarchisten die Regierungskoalition. Die Hilfe aus Nowgorod und Stabilit�t der neuen Regierung f�hrt dazu das schnell Gebietsgewinne gegen die Sklaven verzeichnet werden k�nnen, die Propagandistisch ausgeschlachtet werden. Die Arkadisten und Monarchisten beschlie�en von nun an gemeinsam bei den Wahlen anzutreten und formen somit die Arkadomonarchisten, welche sich als Retter der Landes darstellen k�nnen. Durch die Erfolge im Krieg kommen diese noch st�rker aus den Wahlen. 
1859 ist die Sklavenrevolte zum Gro�teil niedergeschlagen, nur noch vereinzelte Sklavenaufst�ndische leisten Widerstand. Die Beziehung zwischen Nowgorod und Arkadia hat sich durch die Hilfe gebessert und die Bev�lkerung sieht diese nicht mehr als Fremde Invasoren, sondern eher als Freunde die in der Not geholfen haben. 

==Machtergreifung und -Festigung durch die Arkadomonarchisten==
1860-1905: Die Arkadomonarchisten f�hren nach dem B�rgerkrieg einen Kampf gegen die Opposition und weichen den Senat auf. Die Industrialisierung des Landes steht auf Grund dieser Politik fast still.  

ca. 1870: Einf�hrung der Mehrkind Politik zur Steigerung der Bev�lkerung der Arkadier. 

ca. 1923: Bau des Suezkanals in Nowgorodisch-Arkadischer Koorperation, haupts�chlich aber von Nowgorod finanziert und geplant. Die hohe Preis der Durchfahrt f�hrte dazu das Arkadia anfing den Anschluss an die Industrialisierung zu finden. Nowgorod als Erbauer des Kanals wird die Kostenfreie Durchfahrt garantiert. 

==Zeit des Gro�en Krieges==
Arkadia war w�hrend des Krieges selber Neutral, gew�hrte aber Nowgorod, wie im Frieden von Mnogadischu abgemacht, Zugang zu den H�fen, was die Beziehungen mit der Gegenseite anspannte. Die Arkadomonarchisten hatten aber kein Interesse an einer Beteiligung am Krieg nutzen die Ablenkung aber zur Deportation und Gettoisierung der Sklavenbev�lkerung in den S�den des Landes. Zerst�rung von Sklavenkulturgut in Pogromen. Ansiedlung von Arkadier in den K�stenbereichen. Einf�hrung der Regulierungspolitik, zur Bev�lkerungsminderung der Sklaven. 

==Wirtschaftliches Aufholen==
Der Fund der �lquellen um Mitte des 20. Jhd, kann schnell durch das Blut der Sklaven ausgebeutet werden. 

1970: Die Arkadische Bev�lkerung �bersteigt, dank der Regulierungspolitik erstmals seit langem die der Sklaven. Die �ffentliche Aggression gegen diese ist nur noch passiv wahrnehmbar...

Wirtschaftlich war man zwar traditionell stark aufgestellt,fiel ab 1850 jedoch langsam zur�ck da die Industrialisierung nur langssam anzog. Ab der Mitte des 20.Jahrhunderts jedoch sorgten die ergiebigen Erd�lquellen f�r einen starken Devisenzufluss,und das Land wurde einige Jahe lang zur am st�rkten wachsenden Volkswirtschaft der Erde. Sp�testens seit den 90er Jahren ist Arkadien technologisch und wirtschaftlich wieder einer der f�hrenden Staaten auf der Erde,und die Bev�lkerung genie�t einen sehr hohen Lebensstandard.

=Bev�lkerung= 
Im Norden, Nilbereich und K�stenbereich stellen Arkadier den Gro�teil der Bev�lkerung und hat eine hohe Bev�lkerungsdichte. Geringe Nubier/�thiopier Bev�lkerung, generell als Sklaven bezeichnet, die als Haussklaven gehalten werden oder andere unbeliebte Arbeit verrichten. In bestimmten Hafenst�dten Russische Viertel. Andere Ausl�ndische Bewohner, die aber nicht Relevant sind. 
Rest des Landes hat eine Arkadische Elite (Gutsherren etc) und eine gro�e Sklavenbev�lkerung die als Verbrauchsmaterial z�hlen. Relativ geringe Bev�lkerungsdichte.

==Bev�lkerungsentwicklung==
Seit der Einf�hrung der Mehr-Kind-Politik wuchs die Bev�lkerung der Arkadier rasant. Dass Arkadia in keine Konflikte verwickelt war und die im Gro�en Krieg neutral war half hierbei. Die eingef�hrte Regulierungspolitik f�hrte dazu, dass das Wachstum der Sklaven fast stagnierte. Die hohe, durch Zwangsarbeit eingef�hrte Sterberate f�hrte aber schon vorher dazu dass die Sklavenbev�lkerung langsamer wuchs als die gef�rderte Arkadierbev�lkerung. 

{| class="wikitable" 
|- 
! Jahr  !! Arkadier !! Sklaven  
|- 
| 1870 || 20.000.000 || 53.000.000 
|- 
| 1890 || 29.718.900 || 64.670.100
|- 
| 1910 || 44.160.800 || 78.909.800
|-
| 1930 || 65.620.600 || 96.284.900
|- 
| 1950 || 97.508.800 || 117.486.000
|- 
| 1960 || 118.863.000 || 129.778.000
|- 
| 1970 || 144.893.000 || 143.355.000
|- 
| 1980 || 176.624.000 || 146.615.000
|- 
| 1990 || 215.303.000 || 150.322.000
|- 
| 1995 || 237.712.000 || 152.211.000
|- 
| 2000 || 262.453.000 || 154.123.000
|}

>>>>>Ich brauch Daten f�r den gro�en Krieg damit ich bei den Sklaven einen Abstrich in der Bev�lkerung machen kann. 

=Kultur= 
Starke Arkadische Kultur die als Grecoromanisch zu bezeichnen w�re. Die Sklavenkultur wird als unrein angesehen und seit den Postb�rgerkriegischen Pogromen nur noch vereinzelt zu finden.

=Politik=
Die Arkadomonarchisten haben nach dem B�rgerkrieg einen Ein-Parteien-Staat errichtet und s�mtliche Opposition ausgeschalten. Arbeiten mit dem Kaiser zusammen um eine Einheitliche Linie zu f�hren. Apartheid-Regime. Stark Nationalistisch. Mehrkindpolitik und Regulierungspolitik zur Kontrolle des korrekten Bev�lkerungswachstum. Jingoistisch/Hurrapatriotische Milit�rpolitik, zur Wahrung nationaler Interesse, die Interessen anderer werden eher weniger beachtet. Eine Sklavische Terrorgruppe existiert.

=Medien=
==Printmedien==
===Arkadisches Auge:===
Die Parteizeitung der Arkadomonarchisten. In Schrift und Fernsehen vorhanden. Wichtigstes Medium zum Thema Politik.

===Der Seher===
Auflagenm��ig gr��tes Boulevardblatt des Landes. Behandelt allerlei Themen von Politik bis Sport. 

===Siriusstern===
Bekanntestes Hochglanzmagazin des Landes. 

===Leuchtturm von Alexandria===
Bedeutende private Wirtschaftszeitung aus Alexandria. 

===Bibliothek von Alexandria===
Bedeutendes privates Wissenschaftsmagazin aus Alexandria.  

===Der Wachturm===
Staatliche Milit�rzeitung.

=Arkadisches Milit�r=
-Hohe Stellung des Milit�rs
-Wehrpflicht von Mann und Frau f�r 3 Jahre
-Hohe Ausgaben
-Israel als Vorbild

==Purpurne Kaisergarde==
Leibgarde des Kaisers, sch�tzt den Kaiser, den Senat und die Regierung. 

==Heer==
===B�rgerwehr===
Seit w�hrend dem B�rgerkrieg die generelle Mobilmachung ausgerufen wurde, m�ssen alle Arkadier, ob Mann oder Frau, mit 15 Jahren ihren Milit�rdienst ableisten. Die Musterung kann bis zu 2 Jahre verz�gert werden, was meistens voll ausgenutzt wird, weshalb die Regierung plant das Alter direkt auf 17 Jahre zu erh�hen. Die Wehrpflicht dauert 3 Jahre. 

Der Milit�rdienst ist recht hoch in der arkadischen Gesellschaft angesehen weshalb er gew�hnlichen Sklaven verwehrt wird. Nur Haussklaven und Gladiatoren k�nnen, mit Zustimmung ihres Besitzers, einen Milit�rdienst ablegen. 

==Marine==


==Luft==


=Politik=
Die Arkadomonarchisten sind die einzige zugelassene Partei, weshalb sich in ihr als Massenpartei verschiedene politische Str�mungen wiederfinden. Traditionell werden diese als Ecken bezeichnet, was darauf zur�ckzuf�hren ist, dass die urspr�nglichen Parteih�lften sich vom Sprecher in zwei H�lften teilten die beide an den n�chsten Ecken des Raumes nahe sassen. Heutzutage gibt es viele verschiedene Ecken, die sich von Region zu Region in ihrem R�ckhalt unterscheiden. 

==Wahlen==
Auf Landesebene finden keine Wahlen statt, aber auf Kommunalebene wird regelm��ig gew�hlt. Die Wahlen erf�llen die Voraussetzung um als frei, geheim, allgemein und gleich bezeichnet zu werden. Stimm- und Wahlberechtigt sind deshalb nur Personen, die die Arkadische Wehrpflicht erfolgreich absolviert haben, der Besitz von Sklaven f�hrt daher nicht dazu das man weitere Stimmen bekommt. 

==Senat==
Der Senat wird zusammengesetzt aus:

25% werden vom Kaiser direkt auf unbestimmte Zeit bestimmt. 
25% werden von der Regierung direkt auf unbestimmte Zeit bestimmt. 
50% sind als Vertreter der Kommunen im Senat. 

Da die Kommunen durch st�ndige Wahlen nicht stabil bleiben, wird sie durch die Anteile von Regierung und Kaiser stabilisiert.
 
==Kommunen==
In den Kommunen werden regelm��ige Wahlen veranstaltet, welche entscheidend f�r die Politik der Kommunen sind. Um das aktive Wahlrecht zu erhalten muss man den Milit�rdienst erfolgreich absolviert haben, f�r das passive Wahlrecht bestehen weitere Voraussetzungen, so darf man selber kein Blut von Sklaven in sich tragen, was nach Arkadischer Definition bedeutet dass die letzten 3 Generationen frei von Sklaven sein m�ssen. Auch muss man sich aktiv an zwei Wahlen beteiligt haben um selber gew�hlt werden zu k�nnen. 

Bei den Wahlen selber m�ssen die Kandidaten 50% der Stimmen erreichen um eine Kommunalregierung zu bilden. Deshalb gibt es verschiedene Wahlrunden bei denen der Schw�chste Kandidat aus der Wahl ausscheidet und seine Stimmen auf die �brigen Kandidaten verteilt werden. Auf dem Wahlzettel w�hlt man daher nicht nur einen einzigen Kandidaten sondern gewichtet seine Favoriten frei.


=Sklaverei=
==Sklaverei als Grundrecht==
Die Sklaverei wird von den Arkadiern als ein Grundrecht und ein Zeichen von Freiheit angesehen, weshalb viele Arkadier auf den Versuch die Sklaverei abzuschaffen eher mit Unverst�ndnis reagieren. 

==Bedeutung der Sklaverei==
===Wirtschaft===
Die wirtschaftliche Bedeutung der Sklaverei ist nicht mehr so hoch wie fr�her, als die Sklavenarbeit ein wichtiger Grundpfeiler der Industrie war. Heutzutage verdr�ngt die Modernisierung und Technisierung der Industrie mehr und mehr die Sklaven, weshalb die Sklavenbev�lkerung selber am schrumpfen ist. Die schlechte Ausbildung der niederen Sklavenschiwchten f�hrt auch dazu das diese f�r viele moderne Industriezweige einfach unbrauchbar sind. 
Zweige der Wirtschaft in denen die Sklaven heute noch bedeutend sind:
* Infrastruktur
* Feldarbeit und Landwirtschaft
* Arbeit mit giftigen Chemikalien
* Experimentelle-Versuche

===Sport=== 
Die beliebtesten Unterhaltungssportarten des Landes sind auf die Sklaverei angewiesen, dabei k�nnen erfolgreiche Gladiatoren einen angesehen Status erreichen.

===Kritik an der Sklaverei===
Eine Minderheit sieht der Sklaverei eher kritisch gegen�ber, ihrer Meinung nach k�nnten viele Berufe die zur Zeit von Sklaven ausge�bt werden stattdessen von richtigen Arkadiern ausge�bt werden. Dabei teilt sich die Gruppe der "Sklavereigegner" haupts�chlich in zwei Richtungen, die Gem��igten verlangen eine starke Versch�rfung der Regulierungspolitik damit es im Land in einer oder zwei Generation keine Sklaven mehr gibt. Die andere Gruppe, welche von den Gem��igten als Radikal bezeichnet wird, verlangt eine "Aufl�sung der Sklaverei und der Sklaven", also ein Verbot der Sklaverei und eine Vernichtung der Sklaven. Eine Gruppe der "Befreier" existiert nur vereinzelnd und wird sogar von den beiden anderen Gruppen als schlecht informiert bezeichnet und von niemand ernst genommen. 
Von den meisten Arkadiern werden diese Leute eher als Verr�ckte und Extremisten angesehen, weshalb ihr Zuspruch in der Bev�lkerung nur gering ist. In existiert dieses Thema, auf Grund der schlechten Publicity gar nicht.
