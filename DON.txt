{{Staat
|Staatsname - Amtssprache=Donaubund
|Flagge=DON-Flagge.png
|Wappen=DON-Wappen.png
|Karte=DON-Karte.jpeg
|Amtssprache=Deutsch
|Hauptstadt=Wien
|Staatsform=konstitutionelle Erbmonarchie
|Regierungssystem=f�deratives System
|Staatsoberhaupt=Robert Heinrich VI. Palfberger
|Regierungschef=Josef Nechyba
|Gr�ndung=28. Juni 1877 (Gr�ndung des Bundes)
|Aufl�sung=niemals
|Nationalfeiertag=28. Juni
|Nationalhymne=[http://www.youtube.com/watch?v=DcTxGPNwZtc Hymne des Volkes]
|Fl�che=ca. 500.000
|Einwohnerzahl=78.778.000 Einwohner
|Einwohnerdichte=158
|Volksbezeichnung=Donaub�ndler (�sterreicher, Tscheche, Rum�ne, Ungare, S�dslawe)
|Adjektiv=donaul�ndisch
|Religion=Neuapostolische Kirche (53%),<br> Apostolische Kirche (14%),<br> Orthodoxe Ostkirche (27%)
|W�hrung=Schilling
|BIP=
|BIP/Ew=
|Unternehmen=Bundesenergie GesmbH
|Spieler=AuStRa
|Flaggenvorlage=
|Polit. K�rzel={{DON|20}}
|B�ndnis=noch keine
}}

=Geschichte=
==�berblick==
*996 - �sterreich wird unabh�ngig vom bayrischen Herzogtum.
*1182 - Die Habsburger-Dynastie �bernimmt die Macht im neu gegr�ndeten Herzogtum �sterreich.
*1258 - Der erste slawische Krieg gegen die Allianz bestehend aus den M�chten Slowenien, Ungarn und dem, gerade von Ungarn vasallisierten, Serbien f�hrt zur Annexion Sloweniens und der �bernahme Nordkroatiens von Ungarn. Das neu errungene Land wird von �sterreichern besiedelt.
*1272 - Rudolf von Habsburg, Herzog von �sterreich, gelangt auf mysteri�se Weise an die Kaiserkrone es Reiches, ernennt sich selbst zum deutschen K�nig und l�sst sich sp�ter vom Papst zum r�misch-deutschen Kaiser salben. Dies sehen die meisten F�rsten des Reiches als Rechtsbruch und verweigern die Gefolgschaft.
*1272-1294 - Die Zeit des "Drei-K�nigs-Krieges" in dem in verschiedenen Konstellationen um Machtanspr�che in Europa gek�mpft wurde. Am Ende des Krieges wurde das Reich durch einen Vertrag geteilt. Bayern f�llt an �sterreich.
*1302 - Pl�tzliches Sterben des Habsburger Thronfolgers ohne passende Alternative f�hrt zum Machtantritt der Dynastie Palfberger unter Robert Heinrich I.
*1303 - Der ungarische Herrscher plant das Chaos im neuen �sterreichischen Herrscherhaus auszunutzen und erkl�rt den Krieg. Durch schnelle Reaktion gelingt es die ungarischen Streitkr�fte kurz vor Wien in eine, f�r beide Seiten, verlustreiche Schlacht zu verwickeln. Die geschickte Planung des neuen Kaisers f�hrt zum Sieg gegen�ber dem Erzfeind Ungarn und der Annexion Serbiens.
*1502 - Durch den Beginn der Besiedelung der Nordostk�ste S�damerikas wird Kontakt mit den Stammesgemeinschaften der Tupis geschlossen
*1504 - Die Tupi-St�mme schlie�en sich zusammen um die �sterreichischen Siedler zu vertreiben. In den folgenden Jahren werden die stark befestigten �sterreichischen Siedlungen durch Zerm�rbungsangriffe ausgeblutet.
*1523 - Endg�ltiger R�ckzug der Siedler aus S�damerika
*1560 - Beginn des "neunj�hrigen Krieges" gegen die herrschenden Arpaden in Ungarn und Rum�nien
*1569 - Friedensabkommen zwischen der Herrscherfamilie Palfberger und dem slawischen Herrscherhaus der Arpaden f�hrt zu Gebietsgewinnen im Osten (Ungarn, Siebenb�rgen)
*1582 - Die rasante Verbreitung der "neuen Lehre" ausgehend von Krems f�hrt zum Beginn der �sterreichischen Reformation.
*1584 - Die offizielle Gr�ndung der "neuen Apostolischen Kirche" als Gegenpol zur Apostolischen Kirche Zarasiniens.
*1589 - Das Herrscherhaus muss die neue Mehrheitsreligion tolerieren.
*1616 - Das Aussterben der letzten Arpaden f�hrt zur �sterreichischen �bernahme Restrum�niens bis nach Constanta (Schwarzmeerk�ste).
*1821 - Der Bau des ersten Textilmanufaktur in Lubern (real: Ljubljana) leitet den Beginn der �sterreichischen Industrialisierung ein.
*1874 - Es kommt zu wachsenden Revolten aufgrund kultureller und religi�ser Unterschiede im Kaiserreich.
*1876 - Der Kaiser willigt ein sich an eine Konstitution (erstellt von einem Bundesrat von B�rgern mehrer Provinzen) zu halten. Zur �berpr�fung wird eine Instanz am Hof eingesetzt die vom Bundesausschuss gew�hlt wird.
*1877 - Die offizielle Namens�nderung von �sterreich in Donaubund (Monarchie bleibt erhalten) - durch diese Ma�nahme wollte man die Einigkeit im Land erreichen und eine nationale Identit�t entwickeln.
*1903 - Der Kaiser t�tigt massiv gr��ere Investitionen f�r den Wirtschafts- und Infrastrukturausbau (finanziert durch Staatsschulden). 
*1912 - Ausbau der H�fen in Constanta (Schwarzes Meer) und Koper (Mittelmeer) werden in Auftrag gegeben um den �berseehandel besser im Griff zu haben
*1927 - Die Diskriminierung der preu�ischen Minderheit in B�hmen f�hrt zu einem Einmarsch [[Bundesrepublik_Preu�en|Preussens]]. Der Weltkrieg beginnt.
*1934 - Der Krieg endet f�r den Donaubund mit dem Einmarsch in Berlin. Durch den Druck Karthagos und Byzanz wird Preussen ein vertretbares Friedensabkommen diktiert.
*1938 - Das Wirtschaftssystem wird an modernere Standards adaptiert und die erste B�rse wird in Bratislava er�ffnet.
*1944 - Entscheid des Bundesausschusses die Staatsausgaben zu drosseln und die sich vermehrt auf die Schuldenr�ckzahlung zu konzentrieren.
*1951 - Einf�hrung eines Bundesvorstehers als Gegenpol zum Kaiser - er hat neben einem Veto die M�glichkeit Gesetzesvorschl�ge einzubringen

==Mittelalter==
===Gr�ndung des �sterreichischen Herzogtums===
Das Land �sterreich entstand 996 n.Chr. als die damalige Markgrafschaft sich vom Herzogtum Bayern abspaltete. In den folgenden Jahren gab es einen oftmaligen Herrscherwechsel in dem, noch jungen, Land. Am l�ngsten w�hrte die Herrschaft der Familie der Babenberger welche von 1147 bis 1182 an der Macht war. W�hrend ihrer Herrschaft hielten sie gegen starke Einfl�sse aus dem Ausland stand. Neben den Ungarn, die auf die Herrschaft �ber das instabile Land schielten, hatte auch der bayrische Herzog eine Wiedereingliederung im Sinn. Der Graf Rudolf I. und sein Sohn Rudolf II. hatten vor allem durch gerissene B�ndnispolitik das Land zusammen gehalten und einen offenen Konflikt gegen die st�rkeren Nachbarm�chte verhindert. Trotzdem kam es im Jahr 1181 zu Streitigkeiten innerhalb der Herrschaftsfamilie die in der Ermordung des Sohnes von Rudolf II. durch seinen Vetter gipfelte. Diese Schw�che nutzte die aufstrebende Familie der Habsburger und ergriff die Macht. Im Jahre 1182 wurde das Herzogtum �sterreich mit Franz Ferdinand I. als Souver�n ausgerufen.

===Der 1. Slawische Krieg===
Die Herrschaft der Habsburger verlief, trotz Streitigkeiten mit dem ungarischen Herrscherhaus der Arpaden, lange sehr ruhig bis im Jahre 1258 durch einen ung�nstigen diplomatischen Zwischenfall die Spannungen mit den �stlichen Nachbarn in einem Krieg (1. Slawische Krieg) gipfelte. Auf ungarischer Seite standen au�erdem noch die beiden Vasallen Slovenien und Serbien. Am Ende war �sterreich siegreich und erhielt die Herrschaft �ber Slowenien und den Norden Kroatiens. Um das neu errungene Gebiet m�glichst schnell kulturell assimilieren zu k�nnen begann gleich nach Kriegsende die Besiedelung durch �sterreicher.

===Der Fall des Heilig r�mischen Reichs===

===Der 2. Slawische Krieg===
Im Jahre 1302 starb der Thronfolger der Habsburger pl�tzlich bei einem Reitunfall. Da es unter der Dynastie der Habsburger keine passenden Alternativen gab, kam es zu einem Machtkampf mehrere anderer Adelsfamilien, welche den Thron f�r sich beanspruchen wollten. Aus diesem Chaos erhob sich die Familie der Palfberger angef�hrt durch Robert Heinrich Palfberger, der als Robert Heinrich I. die Herrschaft �ber die �sterreichiscen Gebiete antrat. 
Um das Chaos nach seinem Amtsantritt auszunutzen erkl�rte der ungarische Herrscher Ladislaus (Laszlo) I. einige Monate danach den Krieg um die im 1. slawischen Krieg verlorenen Gebiete zur�ckzugewinnen. Doch er untersch�tzte die F�hrungskraft des neuen Monarchen und geriet kurz vor Wien in eine Falle. Im Laufe einer Entscheidungsschlacht gab es auf beiden Seiten gro�e Verluste. Trotzdem hatte die Schlacht kriegsentscheidende Bedeutung, da die Ungarn auf einen schnellen Sieg gesetzt und einen Gro�teil der Truppen direkt nach Wien geschickt hatten. Wenig �berraschend konnte Robert Heinrich einen Gegenschlag gegen Budapest setzen und entschied somit den Krieg f�r sich. Im folgenden Vertrag wird das Gebiet Serbiens, welches ein paar Jahre vor den Ereignissen von den Arpaden annektiert wurde, an �sterreich abgetreten.

==Neuzeit==
===Kolonialismus===
Im Jahre 1502 landeten �sterreichische Siedler erstmals an der Nordostk�ste S�damerikas und nahmen Kontakt mit den eingeborenen St�mmen der Tupi (heute die [[Republik_der_Tup�-V�lker|Republik der Tupi-V�lker]]) auf. Die zun�chst nachbarlich-freundliche Beziehung zwischen den Siedlern und der indigenen Bev�lkerung ver�nderte sich als die �sterreicher damit begannen vereinzelt D�rfer zu �berfallen und die Bewohner zu versklaven. In der Folge schlo�en sich mehrer St�mme der Tupi zusammen um die "Invasoren" zu vertreiben. Ab 1504 wurden vereinzelt europ�ische Siedlungen �berfallen, jedoch waren diese meist zu stark befestigt um von den Eingeborenen erobert zu werden. Trotzdem hatte die Guerilla-Taktik der Tupi eine ersch�pfende Wirkung auf die Siedler. Nach zwei Jahren des Kampfes wurden die Neugr�ndungen von Siedlungen eingestellt um die Verluste zu minimieren.
In den n�chsten Jahren unternahmen die Besucher aus dem Osten alles um sich in den Siedlungsgebieten halten zu k�nnen, doch der Druck war zu gro� und am Ende flohen viele Siedler aus der neuen Welt. Der letzte Siedler verlie� S�damerika im Jahre 1523.

===Der neunj�hrige Krieg===
In den 50er Jahren des 16. Jahrhunderts hatte die Feindschaft zwischen der �sterreichischen Dynastie der Palfberger und des ungarischen Herrscherhauses der Arpaden erneut einen H�hepunkt erreicht. Besonders die r�umliche N�he und die Reibungspunkte am Balkan hatten viel Streitpotential f�r die Kontrahenten zu bieten. Die Erbschaft B�hmens durch den �sterreichischen Souver�n Albrecht III. f�hrte endg�ltig zu einem Konflikt in der Slowakei, welche dem b�hmischen K�nig unterstellt war, zwischen den dort stationierten arpadischen Truppen und den dem �sterreichischen Heer. Dieser regionale Konflikt bl�hte sich schnell zu einem offenen Krieg zwischen �sterreich und dem Arpaden-Reich auf. Nach einer Reihe von Pyrrhussiegen f�r �sterreich und einer Verst�rkung Ungarns durch rum�nische Truppen zeichnete sich ein Patt zwischen den Parteien ab. Im Laufe der n�chsten Jahre verkam der Krieg zu einem steten Hin und Her in dem einmal das ungarische und dann wieder das �sterreichische Milit�r die Oberhand hatte. Nach neun Jahren des Kampfes begannen die ausgebluteten ungarischen Kr�fte unter dem Druck �sterreichs zusammenzubrechen. Nachdem sowohl Budapest als auch Bukarest gefallen waren kapitulierten die Arpaden und ergaben sich ihrem Schicksal. Durch einen vernichtenden Friedensvertrag wurde �sterreich die Herrschaft �ber Ungarn und Siebenb�rgen zugesprochen. Die noch lebenden Familienmitglieder der Arpaden mussten fl�chten und suchten Unterschlupf bei dem F�rsten von Rum�nien dessen Gebiete auf ein Minimum zusammengeschrumpft waren. 

===Die Reformation===
Im Jahr 1582 begann der �sterreichische Theologe und Gelehrte, Rudolf Leber, die Monopolstellung des Pabstes der apostolischen Kirche in Rom anzuzweifeln. Er stellte kritische Fragen und initiierte �ffentliche Diskussionen. Seiner Meinung nach, war die apostolische Kirche zu sehr auf das weltliche konzentriert und richtete sich zu wenig nach den wahren Aussagen der Bibel, sowie deren Zusammenh�nge. In der n�chsten Zeit hielt er regelm�ssige Gespr�chsrunden mit seinen Sch�lern und Interessierten ab und besch�ftigte sich intensiv mit der Bibelauslegung. Durch die langsame, aber stetige, Ausbreitung seiner Theorien, wurden die Abgesandten des Pabstes auf ihn aufmerksam. Als der Pabst davon erfuhr sandte er einen Boten nach Wien, mit einem Schreiben in dem er Druck auf den Kaiser von �sterreich aus�bte. Durch den Versuch des Kaisers, die "H�resie" zu verbieten wurde die Bev�lkerung so sehr aufgewiegelt, dass es zu massiven Unruhen und Protesten kam. 

1584 musste Friedhelm III. einlenken und die Neuapostolische Kirche wurde offiziel gegr�ndet. Seither gab es immer wieder Quereleien mit Zarasinien um die religi�sen Ansichten der B�rger. 

Im Jahr 1889 wurde der neuapostolische Glauben zur Mehrheitsreligion im Reich.

=Politik=
==Provinzen==
Der Donaubund wird formell in 7 gr��ere Provinzen unterteilt:

*�sterreich
*Bayern
*S�dslawien 
*B�hmen/M�hren
*Ungarn 
*Siebenb�rgen
*Wallachei

==Der Bund==
Der Donaubund stellt einen festen Bund der Provinzen unter der Herrschaft der Dynastie Palfberger dar. Der Bund wird repr�sentiert durch den Kaiser und regiert durch den Bundesausschuss und den Bundesvorsteher.

===Der Kaiser===
Die oberste Aufgabe des Kaisers, gegenw�rtig Robert Heinrich VI., ist vor allem die Repr�sentation des Landes. Er vertritt das Land auf internationalen Tagungen (oder bevollm�chtigt Abgesandte), hat das letzte Wort bei au�enpolitischen Verhandlungen und kann sein, nicht unbegrenzt wirksames, Veto bei Gesetzesentw�rfen einbringen. Die Idee hinter dem Kaiser ist es, neben seinen politischen T�tigkeiten, ein Symbol f�r den Wohlstand, die Kultur und die vielf�ltige Geschichte des Donaubundes zu sein. 

===Der Bundesrat===
Der Bundesrat wurde 1876 gegr�ndet und ist urspr�nglich f�r die Kontrolle des Kaisers zust�ndig. Heute ist die oberste Aufgabe die politische Mitsprache der Bev�lkerung der einzelnen Provinzen (Abgeordnete aus jeder Provinz). Der Rat bewegt sich im Rahmen der Verfassung und ber�t �ber Gesetzesentw�rfe und andere politische Ma�nahmen. F�r die Best�tigung eines Gesetzesentwurfes ben�tigt es eine Mehrheit von zwei Drittel. Der Bundesrat kann im �u�ersten Notfall durch den Kaiser aufgel�st werden (der Bundesvorsteher bleibt im Amt), um ihn neu zusammenzusetzen.

===Der Bundesvorsteher===
Der Bundesvorsteher wird alle 5 Jahre aus dem Bundesrat gew�hlt und ist f�r die Kommunikation und die Koordination zwischen dem Rat und dem Kaiser zust�ndig. Er gilt als Gegenpol zur kaiserlichen Macht und hat bei Machtmissbrauch die M�glichkeit den Monarchen auf unbestimmte Zeit abzusetzen.

=Wirtschaft=
Die donaul�ndische Wirtschaft hat in den letzten Jahren stagniert und beginnt jetzt langsam die bestehenden Kapazit�ten auszusch�pfen. Dabei besteht im Osten der Gro�teil der Wirtschaftskraft aus dem Industriesektor, w�hrend der Westen sich auf den Dienstleistungssektor spezialisiert. Der S�den ist haupts�chlich landwirtschaftlich genutzt.

==Land- und Forstwirtschaft==
Der Land- und Forstwirtschaftssektor stellt den gr��ten Anteil in der Provinz S�dslawien. In diesem Gebiet sind rund ein Drittel der Erwerbst�tigen in der Landwirtschaft t�tig. Auch in den Provinzen �sterreich und Bayern ist der Prim�rsektor st�rker vertreten. Durch die besonders stark bewaldeten Gebiete sind diese Provinzen gut f�r die Forstwirtschaft geeignet und k�nnen besonders effektiv zur Jagd genutzt werden.

==Industrie== 
Ein Gro�teil der Schwerindustrie ist in den Provinzen der Wallachei, Siebenb�rgen und Ungarn angesiedelt. Hier wird sich vor allem auf den Maschinenbau und die Metallverarbeitung spezialisiert. Besonders ber�hmt ist die donaul�ndische Produktionsindustrie f�r ihre qualitativ hochwertigen Turbinen.

==Dienstleistungssektor==
Die gr��ten Zentren des Finanzsektors sind die westlich gelegenen St�dte M�nchen und Bratislava.  Ein gro�er Teil der Dienstleistungen besteht auch im Tourismus. Hier sind sowohl die historischen St�dte des gesamten Bundes, als auch die abwechslungsreiche Umwelt ein lohnendes Ziel f�r Touristen.

=Infrastruktur=
==Verkehr==
===Schienenverkehr===
Der Donaubund hat ein noch mittelm��iges Schienennetz, welches im ganzen Reich stetig ausgebaut wird. Hierbei hinken die �stlichen Gebiete in der Entwicklung hinterher. F�r die Erweiterung des Schienennetzes und die Anschlie�ung an ein internationales Schienensystem wurden bereits Verhandlungen mit dem keltischen Unternehmen "Rioga Express Locomotive", kurz REL, gestartet.

===Schiffverkehr===
Der namensgebende Fluss des Bundes, die Donau, tr�gt den Gro�teil des Schiffverkehrs. Besonders f�r den Handel mit den Staaten Byzanz und Gro�-Nowgorod ist der Fluss als Transportm�glichkeit von Bayern bis in das Schwarze Meer ma�geblich. 
Auch der �berseehandel wird durch die ausgebauten H�fen in Constanta und Koper genutzt, hier besteht jedoch noch einiges Potential.

===Stra�enverkehr===

===Luftverkehr===

==Energieversorgung==
Der Bund setzt auf eine saubere und nachhaltige Energieversorgung. Durch die Donau und die Alpen im Westen liegt die Energiegewinnung durch Wasserkraft nahe. Hierzu gibt es ein Netz von Laufkraftwerken an der Donau und ihren Nebenarmen. Dabei wird besonders darauf geachtet, den Schiffverkehr nicht zu behindern. Aus diesem Grund wurden mehrere kleinere Kraftwerke an das Flussufer im Verlauf der Donau erbaut.
Um besser mit Engp�ssen umgehen zu k�nnen besteht ein, noch unausgereiftes, System an Speicherkraftwerken in den Alpen.

Zus�tzlich zur Wasserkraft ist auch die Windkraft, besonders in den flacheren Gebieten des Bundes, ein Thema. Im Westen des Landes werden bereits viele Ebenenlandschaften von Windr�dern dominiert. Der Osten ist auch hier hinterher.

Trotzdem reicht die Energieproduktion im Inland nicht um den Stromverbrauch des Bundes abzudecken. Aus diesem Grund wird (dreckiger) Strom aus L�ndern wie [[Karthagisches_Reich|Karthago]] oder [[Kaisertum_von_Neu-Byzanz|Byzanz]] importiert.

=Bev�lkerung=
==Kultur==
Die Bev�lkerung des Donaubundes h�ngt einer Vielzahl verschiedener Ethnien an. Seit der zweiten H�lfte des 19. Jahrhunderts setzt der Staat auf eine sehr tolerante Politik im Umgang mit dieser Diversit�t. Das langj�hrige Zusammenleben im Donaubund hat eine Art Mischkultur hervorgebracht, welche besonders in den Grenzregionen zwischen den einzelnen Provinzen des Landes stark bemerkbar ist. Trotzdem bringt der stark ausgepr�gte Pluralismus auch Nachteile mit sich, beispielsweise in den s�dslawischen Gebieten am Balkan (insbesondere Serbien). Hier gibt es vereinzelt noch immer Unabh�ngigkeitsbewegungen, jedoch erkennt der Gro�teil der Bev�lkerung die Vorteile welche der Bund bringt an.

==Religion==
Die Bev�lkerung des Bundes ist vorallem durch den Christentum gepr�gt. Nur 6% der Menschen h�ngt einem nicht christlichen Glauben an. Der Rest teilt sich in die drei gro�en christlichen Religionen auf: die Apostolische Kirche (14%), die neue Apostolische Kirche (53%) und die orthodoxe Ostkirche (27%). Dabei stellt aber die Orthodoxie den Gro�teil der Bev�lkerung in Serbien und Rum�nien w�hrend sich die Menschen in Tirol und Vorarlberg noch haupts�chlich nach dem Papst richten. Der Rest des Reiches wurde stark durch die Reformation im 16. Jahrhundert gepr�gt und h�ngt auch heute noch dieser Konfession an.
Durch die Religionsfreiheit, welche im 20. Jahrhundert Einzug hielt beginnen sich langsam auch kleinere Freikirchen im Bund zu verbreiten. Diese haben jedoch noch eine so geringe Mitgliederanzahl, dass es kaum f�r 1% reicht.

=Symbolik=
==Kaiserliche Fahne==
Die Flagge des Bundes soll die Einigkeit und den Zusammenhalt ihrer B�rger darstellen. Der rote L�we symbolisiert die Dynastie der Palfberger und ihre Wacht �ber �sterreich und sp�ter �ber den Bund. Die Bev�lkerung und ihre Diversit�t wird durch den zweik�pfigen Adler symbolisiert, welcher gleichzeitig ein Bildnis der Freiheit darstellt. Die Hintergrundfarben schwarz und Gold sind die Farben des Reiches. Sie stellen die Sch�nheit und Vielf�ltigkeit der Natur sowie der St�dte des Bundes dar.
