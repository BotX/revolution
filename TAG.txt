{{Staat
|Staatsname - Amtssprache= Tagairt Mac T�re
|Staatsname - Transkription=
|Staatsname - Deutsch= Silber Wolf
|Flagge=TAG-Flagge.png
|Wappen=?-Wappen.png
|Karte=Karte_Politik_S%C3%BCdamerika_K.jpeg
|Amtssprache= Irisch
|Hauptstadt= nua Corcaigh ( Neu Cork)
|Staatsform= Republik
|Regierungssystem= Milit�r-Diktatur mit Pr�sidenten (Amt wird vererbt)
|Staatsoberhaupt= Pr�sident Dan O'Connacht
|Regierungschef= Pr�sident Dan O'Connacht
|Gr�ndung= 1862 Unabh�ngigkeit
|Aufl�sung=
|Nationalfeiertag= 25.06.
|Nationalhymne=
|Fl�che= 3.367.980 km�
|Einwohnerzahl= 43.000.000
|Einwohnerdichte= 12,77 / km�
|Volksbezeichnung= Tagairten (Silbernen)
|Adjektiv=
|Religion= keltische G�ttersaga
|W�hrung= T�re
|BIP= 463 Mrd. T�re
|BIP/Ew= 10.767 T�re / Ew
|Unternehmen=
|Spieler= ShadowHF
|Flaggenvorlage=
|Polit. K�rzel={{TAG}} TAG
|B�ndnis=
}}

== Geschichte ==

=== Zeit vor der Kolonisierung ===

Die fr�hen Jahre sind nicht genau festgehalten. Auch gab es nicht wirklich Grenzen wie aus heutiger Sicht bestimmt. Die Einwohner lebten in Verb�nden zusammen, die meist auf drei bis vier Familien bestanden. Aufgrund der Flora und Fauna waren diese teilweise sesshaft.

Es gibt �berlieferungen die besagen, dass nur geringer Kontakt zwischen diesen Gruppen bestand. Dies war vermutlich einer der Gr�nde, warum die Kolonisierung durch die Cluain ziemlich reibungslos verlie�.

=== Kolonisierung ===

Die ersten Schiffe landeten 1786 an der Atlantik-K�ste. Die Neu-Ank�mmlinge wurden freundlich begr��t und es begann eine friedliche Ko-Existenz in der die Kulturen sich integrierten und vermischten. Es kam nur zu vereinzelten kriegerischen Auseinandersetzungen, als die Cluain Siedler in heiliges Gebiet gehen wollten.

=== Unabh�ngigkeit ===

Im Januar 1862 kam es zu einem folgenschweren Eklat, als der cluainische Aussenminister die Einwohner von Tagairt Mac T�re als minderwertig bezeichnete. Es kam zu Demonstrationen in deren Verlauf das Milit�r der Kolonialmacht diese blutig beendete. Danach schwelte der Aufstand im Untergrund und die Kolonialmacht sah sich immer h�ufiger Partisanen-Angriffen ausgesetzt. Am 25.06. 1862 zog die Regierung die Notbremse und entliess Tagairt Mac T�re in die Unabh�ngigkeit. Als Gegenleistung verblieb das Land jedoch als Mitglied in der Vereinigung mit der Kolonialmacht.

=== Neu-Zeit ===

Tagairt Mac T�re florierte seit der Unabh�ngigkeit aufgrund der Erschliessung von Bodensch�tzen, wie Metallen, Erd�l und Erdgas. Zum Schutz der heimischen Wirtschaft wurden seit Mitte des 20. Jahrhunderts Schutzz�lle erhoben auf alle landwirtschaftlichen G�ter. In den Grenzst�dten sind das Milit�r und die Sondereinheiten der Polizei ein st�ndiges und vertrautes Bild. 
Tourismus konnte sich nie wirklich etablieren, da auch die Regierung den Fokus auf andere Bereiche gelegt hat.

Anfang April 2001 kam es zu einem Milit�rputsch, als der Pr�sident das Parlament aufl�ste und die Verfassung f�r ung�ltig erkl�rte. Unterst�tzt wurde er von dem Milit�r des Landes.

== Wirtschaft ==

=== Landwirtschaft ===

Das Land verf�gt �ber einen gro�en Agrar-Sektor und sch�tzt diesen mit allen Mitteln. Aufgrund dieses Schutzes, m�ssen die Bauern keinen Preiskampf bef�rchten und erhalten staatliche Subventionen.

=== Energie-Sektor ===

Tagairt Mac T�re setzt �berwiegend auf �l- und Gaskraftwerke, da diese Ressourcen zur Gen�ge im Land vorhanden sind und nicht teuer importiert werden m�ssen. Forschungen im Bereich der Kernspaltung wurden nicht aktiv verfolgt. Der Sektor der alternativen Stromerzeugung ist klein und wird nicht besonders durch die Regierung gef�rdert.

=== Industrie ===

Hauptsektor ist die Erd�l-Industrie und der Export von den Erzeugnissen und dem Erdgas. Auch ist der Bergbau immer noch ein Sektor, der gef�rdert wird, auch wenn die Ressourcen nicht ausreichen, um den Eigenbedarf komplett zu decken.

=== Tourismus ===

Dieser Sektor ist nicht entwickelt und nur eine Handvoll Menschen kommen in das Land, meistens um Abenteuer-Urlaub zu machen. Es gibt kaum Infrastruktur und die Urlauber sind auf die Hilfe der Einheimischen angewiesen.

=== Arbeitslosigkeit ===

Die Arbeitslosigkeit liegt bei ca. 18%. Vor allem Jugendarbeitslosigkeit ist verbreitet. Viele nutzen die M�glichkeit in der Armee zu dienen und verpflichten sich f�r mehrere Jahre. Auf dem Land ist die Arbeitslosigkeit geringer, da die Menschen in den landwirtschaftlichen Betrieben arbeiten. Die Dunkelziffer wird auf ca. 22-24% gesch�tzt.


== Demographie ==

=== Verteilung Reichtum ===

Die Verteilung der Geldmenge ist sehr einseitig. Die oberen 10% besitzen fast 90% des Reichtums. Die Bev�lkerung hat sich arrangiert und kommt vor allem mit Tauschhandel auf dem Land �ber die Runden.

=== Altersstruktur ===

Das Land hat eine Lebenserwartung von rund 67 Jahren f�r M�nner und 65 Jahren f�r Frauen. Gerade in den l�ndlichen Gegenden leben die Alten als gesch�tzte Mitglieder und werden vom Dorf versorgt. In den St�dten hat sich dies gewandelt und die Menschen werden abgeschoben in spezielle Heime, die diese verpflegen.

== Milit�r ==

=== Allgemeines ===

Es besteht eine allgemeine Wehrpflicht von 18 Monaten. Man kann sich l�nger verpflichten. Im Durchschnitt dienen die Soldaten 36 Monate aufgrund der angespannten wirtschaftlichen Situation.

=== Heer ===

Entwicklung zur Verst�rkung der Panzerwaffe wurde ein Prototyp fertiggestellt, der nun in Serie gehen wird.
[[Datei:CV90.jpg]]

Der Gro�teil des Heeres besteht aus normalen Soldaten. Es gibt nur wenige Spezialeinheiten. 

=== Marine ===

Die Marine besteht aus 4 Zerst�rern und einigen Begleitschiffen. Es gibt keine Tr�gerflotten.

=== Luftwaffe ===

Tagairt nennt 20 Kampfflugzeuge sein eigen, sowie 10 Bomber.
